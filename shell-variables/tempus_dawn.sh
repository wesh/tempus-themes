#!/bin/bash
# Can be sourced from scripts to expose these colours as variables.

# theme: Tempus Dawn
# author: Protesilaos Stavrou (https://protesilaos.com)
# description: Light theme with a soft, slightly desaturated palette (WCAG AA compliant)
background="#f3f4f6"
foreground="#525470"
backgroundalt="#e6e7ea"
foregroundalt="#705e4d"
cursorColor="#525470"
cursorColor2="#f3f4f6"
color0="#525470"
color1="#9b3132"
color2="#306130"
color3="#73500a"
color4="#4c547e"
color5="#883c64"
color6="#186060"
color7="#e6e7ea"
color8="#705e4d"
color9="#9b474d"
color10="#4e6938"
color11="#8e5319"
color12="#5c5d8c"
color13="#a24055"
color14="#2d6978"
color15="#f3f4f6"

# Special colours
backgrounddim="#ecedf0"
